### Minimalistic Finite State Machine

Getting started

```js
import { createMachine, type StateObject } from "fsm";

const states: StateObject[] = [
  {
    state: "idle",
    on: {
      start: "starting",
    },
  },
  // multiple states
  {
    state: "starting",
    on: {
      starting_done: "started",
      start_error: "error",
    },
  },
  {
    state: "started",
    on: {
      stop: "stopping",
    },
  },
  {
    state: "stopping",
    on: {
      stopping_done: "idle",
    },
  },
  {
    state: "error",
    on: {
      restart: "idle",
    },
  },
];

const fsm = createMachine(states);
console.log("currentState", fsm.current); // idle

fsm.dispatch("start");
console.log("currentState", fsm.current); // starting

fsm.dispatch("starting_done");
console.log("currentState", fsm.current); // started

// reset to any state or initial state by default
fsm.reset("idle");
console.log("currentState", fsm.current); // idle
```
