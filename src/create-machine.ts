import { transformStates } from "./state";
import { StateService, Options } from "./types";

export function createMachine<S extends string | number>(
  states: Options<S>
): StateService<S> {
  return transformStates(states);
}
