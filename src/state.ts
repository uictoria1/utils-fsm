import {
  StateService,
  Options,
  StateTransition,
  SubscribeFunction,
} from "./types";

function validateState<S>(state: S): boolean {
  return state !== null && state !== undefined;
}

function validateStateTransition<S>(_transition: StateTransition<S>): boolean {
  Object.keys(_transition).forEach((event) => {
    if (
      event === undefined ||
      event === null ||
      typeof event !== "string" ||
      !event
    ) {
      throw new Error("Invalid transition object");
    }

    const state = _transition[event];
    if (
      state === undefined ||
      state === null ||
      (typeof state !== "string" && typeof state !== "number")
    )
      throw new Error("Invalid transition object");

    if (typeof state === "string" && !state) {
      throw new Error("Invalid transition object");
    }
  });
  return true;
}

function validateStates<S extends string | number>(options: Options<S>) {
  const stateKeys = options.states.map((e) => e.state);
  const uniqueStateKeys = new Set(stateKeys);
  if ([...uniqueStateKeys].length !== stateKeys.length)
    throw new Error(
      "Invalid state keys. Defined: " +
        stateKeys.length +
        ". But unique count: " +
        [...uniqueStateKeys].length
    );

  for (let i = 0; i < options.states.length; i++) {
    const stateTransition = options.states[i].on;
    validateStateTransition(stateTransition);
  }
  return true;
}

function getInitialState<S extends string | number>(states: Options<S>) {
  if (validateState(states.initial)) return states.initial;

  const _inferredInitialState = states.states[0]?.state;
  if (_inferredInitialState !== undefined && _inferredInitialState !== null)
    return _inferredInitialState;

  throw new Error(
    "Initial state is not defined or can't be inferred from states"
  );
}

function _transformStates<S extends string | number>(
  options: Options<S>
): StateService<S> {
  // get initial state
  const _initialState = getInitialState(options);

  const service: StateService<S> = {
    initialState: _initialState,
    current: _initialState,
    stateObjects: options.states,
    subscriptions: [],
    dispatch: function (event: string) {
      const oldState = this.current;
      const stateObject = this.stateObjects.find(
        (e) => e.state === this.current
      );
      if (!stateObject) {
        console.warn("State object for current state is not defined");
        return false;
      }

      const newState = stateObject.on[event];
      if (newState === undefined) {
        console.warn("State for event: '" + event + "' is not defined");
        return false;
      }

      if (this.current === newState) return false;

      this.current = newState;
      if (this.subscriptions.length) {
        this.subscriptions.forEach((callback) => callback(newState, oldState));
      }

      return true;
    },
    reset: function (state?: S) {
      if (state) {
        const uniqueStates = [
          ...new Set(this.stateObjects.map((e) => e.state)),
        ];
        if (uniqueStates.includes(state)) {
          this.current = state;
        } else {
          return false;
        }
      } else {
        this.current = this.initialState;
      }
      return true;
    },
    subscribe: function (callback: SubscribeFunction<S>) {
      if (callback && typeof callback === "function")
        this.subscriptions.push(callback);
    },
    getStates() {
      const states = this.stateObjects.map((e) => e.state);
      return [...new Set(states)];
    },
  };
  return service;
}

export function transformStates<S extends string | number>(
  states: Options<S>
): StateService<S> {
  validateStates(states);
  return _transformStates(states);
}
