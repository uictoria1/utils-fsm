export interface StateTransition<S> {
  [key: string]: S;
}

export interface StateObject<S> {
  state: S;
  order?: number;
  on: StateTransition<S>;
}

type Dispatch = (event: string) => boolean;
type Reset<S> = (state?: S) => void;
export type SubscribeFunction<S> = (newState: S, oldState?: S) => void;

export interface StateService<S> {
  initialState: S;
  current: S;
  stateObjects: StateObject<S>[];
  subscriptions: SubscribeFunction<S>[];
  subscribe: (callback: SubscribeFunction<S>) => void;
  dispatch: Dispatch;
  reset: Reset<S>;
  getStates: () => S[];
}

export interface Options<S extends string | number> {
  initial?: S;
  states: StateObject<S>[];
}
