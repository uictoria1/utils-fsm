import { describe, it, expect, vi } from "vitest";
import { createMachine, StateService, Options } from "../src";
import { StateObject } from "../src/types";

enum testEvent {
  next = "next",
  prev = "prev",
}

enum testState {
  idle = "idle",
  coding = "coding",
  testing = "testing",
  passed = "passed",
  failed = "failed",
  refactoring = "refactoring",
}

const states: Options<testState> = {
  initial: testState.idle,
  states: [
    {
      state: testState.idle,
      on: {
        [testEvent.next]: testState.coding,
      },
    },
    {
      state: testState.coding,
      on: {
        [testEvent.next]: testState.testing,
        [testEvent.prev]: testState.idle,
      },
    },
    {
      state: testState.testing,
      on: {
        [testEvent.next]: testState.passed,
        [testEvent.prev]: testState.failed,
      },
    },
    {
      state: testState.passed,
      on: {
        [testEvent.next]: testState.refactoring,
      },
    },
    {
      state: testState.failed,
      on: {
        [testEvent.next]: testState.coding,
      },
    },
    {
      state: testState.refactoring,
      on: {
        [testEvent.next]: testState.coding,
      },
    },
  ],
};

describe("fsm test", () => {
  it("empty states", () => {
    expect(() => {
      createMachine({ states: [] });
    }).toThrow(
      new Error("Initial state is not defined or can't be inferred from states")
    );
  });

  it("initialize fsm with return object", () => {
    const fsm = createMachine(states);

    expect(fsm).toBeInstanceOf(Object);
    expect(fsm).toHaveProperty("current");
    expect(fsm).toHaveProperty("subscribe");
    expect(fsm).toHaveProperty("dispatch");
    expect(fsm).toHaveProperty("stateObjects");
    expect(fsm).toHaveProperty("initialState");

    expect(fsm.dispatch).toBeInstanceOf(Function);
  });

  it("invalid initial state", () => {
    const fsm = createMachine({
      initial: "invalid",
      states: [
        {
          state: "a",
          on: {
            a: "a",
          },
        },
      ],
    });
    expect(fsm.dispatch("a")).toBeFalsy();
  });

  it("fsm should have states", () => {
    const fsm = createMachine(states);

    expect(fsm.stateObjects).toBeDefined();
    expect(fsm.stateObjects).toBeInstanceOf(Object);
    expect(Array.isArray(fsm.stateObjects)).toBeTruthy();
  });

  it("getStates return all unique states", () => {
    const fsm = createMachine(states);
    expect(fsm.getStates()).toEqual([
      testState.idle,
      testState.coding,
      testState.testing,
      testState.passed,
      testState.failed,
      testState.refactoring,
    ]);
  });

  it("fsm should have initial state", () => {
    const fsmWithoutInitial = createMachine(states);
    expect(fsmWithoutInitial.initialState).toBe(testState.idle);

    const fsmWithInitial2 = createMachine({
      initial: testState.coding,
      states: states.states,
    });
    expect(fsmWithInitial2.initialState).toBe(testState.coding);
  });

  it("fsm transition throws error if invalid event or target state is specified", () => {
    expect(() => {
      createMachine({
        states: [
          {
            state: testState.coding,
            on: {
              "": testState.failed,
            },
          },
        ],
      });
    }).toThrow(new Error("Invalid transition object"));
    expect(() => {
      createMachine({
        states: [
          {
            state: testState.coding,
            on: {
              // @ts-ignore for sake of test
              [testEvent.next]: "",
            },
          },
        ],
      });
    }).toThrow(new Error("Invalid transition object"));
  });

  it("fsm current state should be equal to initial state after initialization", () => {
    const fsm = createMachine(states);
    expect(fsm.current).toBe(fsm.initialState);
  });

  it("fsm dispatch function should change current from initialState to target state", () => {
    const fsm = createMachine(states);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.coding);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.testing);
  });

  it("fsm dispatch function should not change current from initialState to target state if there is no link", () => {
    const fsm = createMachine(states);

    fsm.dispatch(testEvent.prev);
    expect(fsm.current).toBe(testState.idle);

    fsm.dispatch(testEvent.prev);
    expect(fsm.current).toBe(testState.idle);
  });

  it("fsm should pass all states", () => {
    const fsm = createMachine(states);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.coding);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.testing);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.passed);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.refactoring);

    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.coding);
  });

  it("fsm should call callback when specified", () => {
    const fsm = createMachine(states);
    const callback = {
      callback: function (): any {
        return undefined;
      },
    };
    const callbackSpy = vi.spyOn(callback, "callback");

    fsm.dispatch(testEvent.next);
    expect(callbackSpy).not.toBeCalled();

    fsm.subscribe(() => {
      callback.callback();
    });
    expect(callbackSpy).not.toBeCalled();

    fsm.dispatch(testEvent.next);
    fsm.dispatch(testEvent.next);
    expect(callbackSpy).toBeCalledTimes(2);
  });

  it("fsm should throw error if callback throws error", () => {
    ``;
    const fsm = createMachine(states);
    const callback = {
      callback: function (): any {
        throw new Error("callback error");
      },
    };
    fsm.subscribe(() => {
      callback.callback();
    });
    expect(() => {
      fsm.dispatch(testEvent.next);
    }).toThrow(new Error("callback error"));
  });

  it("fsm callback should change outer variable", () => {
    const fsm = createMachine(states);
    let outerVar = 0;
    const changeCallback = () => {
      outerVar = outerVar + 1;
    };
    fsm.subscribe(() => {
      changeCallback();
    });

    fsm.dispatch(testEvent.prev);
    expect(outerVar).toBe(0);

    fsm.dispatch(testEvent.next);
    expect(outerVar).toBe(1);

    fsm.dispatch(testEvent.next);
    expect(outerVar).toBe(2);
  });

  it("dispatching defined event for state that doesn't change state", () => {
    const fsm = createMachine({
      states: [
        {
          state: testState.idle,
          on: {
            [testEvent.next]: testState.idle,
          },
        },
      ],
    });
    expect(fsm.dispatch(testEvent.next)).toBe(false);
  });

  it("resetting unknown state doesn't change state", () => {
    const fsm = createMachine(states);

    // @ts-ignore for sake of test
    expect(fsm.reset("some unknown state")).toBe(false);
    expect(fsm.current).toBe(testState.idle);
  });

  it("resetting state without parameter sets to initial value", () => {
    const fsm = createMachine(states);
    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.coding);

    fsm.reset();
    expect(fsm.current).toBe(testState.idle);
  });

  it("resetting state with parameter sets to the value", () => {
    const fsm = createMachine(states);
    fsm.dispatch(testEvent.next);
    expect(fsm.current).toBe(testState.coding);

    fsm.reset(testState.failed);
    expect(fsm.current).toBe(testState.failed);
  });

  it("duplicate state object", () => {
    const states = {
      states: [
        {
          state: "1",
          on: {
            next: "2",
          },
        },
        {
          state: "1",
          on: {
            next: "2",
          },
        },
      ],
    };
    expect(() => {
      createMachine(states);
    }).toThrow("Invalid state keys. Defined: 2. But unique count: 1");
  });

  it("test fsm for readme example", () => {
    const states: StateObject<string>[] = [
      {
        state: "idle",
        on: {
          start: "starting",
        },
      },
      {
        state: "starting",
        on: {
          starting_done: "started",
          start_error: "error",
        },
      },
      {
        state: "started",
        on: {
          stop: "stopping",
        },
      },
      {
        state: "stopping",
        on: {
          stopping_done: "idle",
        },
      },
      {
        state: "error",
        on: {
          restart: "idle",
        },
      },
    ];

    const fsm = createMachine({ states });
    expect(fsm.current).toBe("idle");

    fsm.dispatch("start");
    expect(fsm.current).toBe("starting");

    fsm.dispatch("starting_done");
    expect(fsm.current).toBe("started");

    fsm.dispatch("stop");
    expect(fsm.current).toBe("stopping");

    fsm.dispatch("stopping_done");
    expect(fsm.current).toBe("idle");

    fsm.dispatch("start");
    expect(fsm.current).toBe("starting");

    fsm.dispatch("start_error");
    expect(fsm.current).toBe("error");

    fsm.dispatch("restart");
    expect(fsm.current).toBe("idle");
  });
});
