import { defineConfig } from "vite";
import { resolve } from "path";
import dts from 'vite-plugin-dts';

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, "src/index.ts"),
      name: "fsm",
      fileName: "fsm",
    },
  },
  plugins: [dts()],
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
});
